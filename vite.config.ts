import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import transformHtmlPlugin from "./plugins/transformHtmlPlugin";
import { createSvg } from "./src/components/SvgIcons/index";

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    createSvg("./src/assets/svg/"),
    transformHtmlPlugin({
      APP_TITLE: "Hello World",
      META_DESCRIPTION: "一个基础的 Vue3.x 麻雀示例",
    }),
  ],
  resolve: {
    alias: {
      "@": "/src",
    },
  },
});
