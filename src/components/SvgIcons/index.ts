import { readFileSync, readdirSync, Dirent } from "fs";
import { Plugin } from "vite";

let idPrefix = "";
const svgTitle = /<svg([^>+].*?)>/;
const clearHeightWidth = /(width|height)="([^>+].*?)"/g;
const hasViewBox = /(viewBox="[^>+].*?")/g;
const clearReturn = /(\r)|(\n)/g;

function processSvgContent(content: string, fileName: string): string {
  let width = 0;
  let height = 0;

  content = content
    .replace(clearReturn, "")
    .replace(svgTitle, (_match, attributes: string) => {
      let newAttributes = attributes.replace(
        clearHeightWidth,
        (_match: string, attr: string, value: string) => {
          if (attr === "width") width = Number(value);
          else if (attr === "height") height = Number(value);
          return "";
        }
      );

      if (!hasViewBox.test(attributes)) {
        newAttributes += `viewBox="0 0 ${width} ${height}"`;
      }

      return `<symbol id="${idPrefix}-${fileName.replace(
        ".svg",
        ""
      )}" ${newAttributes}>`;
    });

  return content.replace("</svg>", "</symbol>");
}

function svgFind(directory: string): string[] {
  const svgFiles: string[] = [];

  const dirents: Dirent[] = readdirSync(directory, { withFileTypes: true });
  for (const dirent of dirents) {
    if (dirent.isDirectory()) {
      svgFiles.push(...svgFind(`${directory}${dirent.name}/`));
    } else if (dirent.name.endsWith(".svg")) {
      const svgContent = readFileSync(`${directory}${dirent.name}`, "utf-8");
      const processedContent = processSvgContent(svgContent, dirent.name);
      svgFiles.push(processedContent);
    }
  }

  return svgFiles;
}

export const createSvg = (
  path: string,
  prefix = "icon"
): Plugin | undefined => {
  if (!path) return;

  idPrefix = prefix;
  const svgSymbols = svgFind(path);

  return {
    name: "svg-transform",
    transformIndexHtml(html: string): string {
      const svgSprite = `<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" style="position: absolute; width: 0; height: 0">${svgSymbols.join(
        ""
      )}</svg>`;
      return html.replace("<body>", `<body>${svgSprite}`);
    },
  };
};
