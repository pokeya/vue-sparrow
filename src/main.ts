import { createApp } from "vue";
import App from "./App.vue";
import SvgIcon from "@/components/SvgIcons/index.vue";
import "./style.css";

const app = createApp(App);
app.component("SvgIcon", SvgIcon);
app.mount("#app");
